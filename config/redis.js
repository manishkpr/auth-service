/**
 * Module dependencies.
 */
const Bluebird = require('bluebird');
const redis = require('redis');

Bluebird.promisifyAll(redis.RedisClient.prototype);
Bluebird.promisifyAll(redis.Multi.prototype);

const { print } = require('../config/pino');

const redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);


redisClient.auth('');

redisClient.on('connect', () => {
  print(`Redis connected to ${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`);
});

redisClient.on('error', (err) => {
  print(`Redis error: ${err}`);
});


module.exports = { redisClient, redis };
