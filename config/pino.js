const pino = require('pino');

const logger = pino({
  prettyPrint: true,
});

function print(message) {
  if (process.env.LOG_ENABLED || false) {
    logger.info(message);
  }
}

module.exports = { logger, print };
