/**
 * Module dependencies.
 */
const mongoose = require('mongoose');
const { print } = require('../config/pino');

let mongoDB;

const options = {
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

// require database URL from properties file
const DB = (process.env.NODE_ENV === 'dev') ? process.env.MONGO_DB : process.env.MONGO_TEST_DB;
const dbURL = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${DB}`;
print(dbURL);

function setMongoConnection(connection) {
  mongoDB = connection;
}

function getMongoConnection() {
  return mongoDB;
}

// export this function and imported by server.js
function startMongo() {
  mongoose.connect(dbURL, options);

  mongoose.connection.on('connected', () => {
    print('Mongoose default connection is open to ', dbURL);
  });

  mongoose.connection.on('error', (err) => {
    print(`Mongoose default connection has occurred ${err} error`);
  });

  mongoose.connection.on('disconnected', () => {
    print('Mongoose default connection is disconnected');
  });

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      print('Mongoose default connection is disconnected due to application termination');
      process.exit(0);
    });
  });

  setMongoConnection(mongoose.connection);
}

function stopMongo() {
  mongoose.disconnect();
}

module.exports = {
  startMongo,
  stopMongo,
  getMongoConnection,
};
