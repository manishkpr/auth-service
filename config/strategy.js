const LocalStrategy = require('passport-local').Strategy;
const passport = require('koa-passport');
const UserDao = require('../api/controllers/dao/users');
const Password = require('../api/controllers/decorator/password');


const strategy = new LocalStrategy(
  (async (email, password, done) => {
    const user = await UserDao.isUserExist(email);

    await Password.comparePassword(password);

    return done(null, user);
  }),
);

function isLocalAuthenticated() {
  return passport.authenticate('local', { session: false });
}

module.exports = { strategy, isLocalAuthenticated };
