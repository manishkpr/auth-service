import express from 'express';

function init() {
  const app = express();
  app.use(express.json());

  app.get('/', (req, res) => res
    .status(200)
    .send({ message: 'Ok!' }));

  return app;
}

module.exports.init = init;
