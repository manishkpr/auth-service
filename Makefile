start:
	docker-compose up -d

verbose:
	docker-compose up

build:
	docker-compose build

stop:
	docker-compose down