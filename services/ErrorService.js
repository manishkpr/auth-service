/* eslint-disable max-classes-per-file */
// Utilities
const ApiError = require('../utils/ApiError');

class MissingRequestParameter extends ApiError {
  constructor(parameterName) {
    let message = 'Mandatory request parameter is missing';

    message = parameterName ? `${message}: '${parameterName}'` : message;

    super(message, 400, 'C-1010');
    this.name = this.constructor.name;
  }
}

class InvalidEmailError extends ApiError {
  constructor(parameterName) {
    let message = 'Invalid email';
    message = parameterName ? `${message}: '${parameterName}'` : message;
    super(message, 400, 'C-1011');
    this.name = this.constructor.name;
  }
}

class UserAlreadyExist extends ApiError {
  constructor(parameterName) {
    let message = 'Already exist';
    message = parameterName ? `${message}: '${parameterName}'` : message;
    super(message, 400, 'C-1012');
    this.name = this.constructor.name;
  }
}

class UserNotFound extends ApiError {
  constructor(parameterName) {
    let message = 'User/Email not found';
    message = parameterName ? `${message}: '${parameterName}'` : message;
    super(message, 400, 'C-1013');
    this.name = this.constructor.name;
  }
}

class InvalidUserPassword extends ApiError {
  constructor(parameterName) {
    let message = 'Invalid User/Password';
    message = parameterName ? `${message}: '${parameterName}'` : message;
    super(message, 401, 'C-1014');
    this.name = this.constructor.name;
  }
}

class InvalidToken extends ApiError {
  constructor(parameterName) {
    let message = 'Invalid token';
    message = parameterName ? `${message}: '${parameterName}'` : message;
    super(message, 400, 'C-1015');
    this.name = this.constructor.name;
  }
}

class RedisError extends ApiError {
  constructor(err) {
    const message = `Error occured while accessing the database :: ${err}`;

    // TODO Log `err` separately.

    super(message, 500, 'DI-5100');
    this.name = this.constructor.name;
  }
}

class RedisInvalidParamError extends ApiError {
  constructor(parameter) {
    const message = `Invalid parameter: ${parameter}`;

    super(message, 500, 'DI-5101');
    this.name = this.constructor.name;
  }
}

class Error extends ApiError {
  constructor(err) {
    const message = `Error occurred :: ${err}`;
    super(message, 500, 'C-5000');
    this.name = this.constructor.name;
  }
}

module.exports = {
  MissingRequestParameter,
  InvalidEmailError,
  UserAlreadyExist,
  UserNotFound,
  InvalidUserPassword,
  InvalidToken,
  RedisError,
  RedisInvalidParamError,
  Error,
};
