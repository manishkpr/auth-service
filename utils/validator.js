/* eslint-disable no-restricted-globals */
/* eslint-disable prefer-destructuring */
const Validator = require('validator');
// Services
const ErrorService = require('../services/ErrorService');

module.exports = {
  async validateUserInfo(param, options = { ignoreName: false }) {
    const { name, email, password } = param;

    if (!name && !options.ignoreName) { throw new ErrorService.MissingRequestParameter('name'); }

    if (!email) { throw new ErrorService.MissingRequestParameter('email'); }

    if (!Validator.isEmail(email)) {
      throw new ErrorService.InvalidEmailError('email');
    }

    if (!password) { throw new ErrorService.MissingRequestParameter('password'); }

    return true;
  },

  async validatePayloadAndTTL(payload) {
    const ttl = Number(process.env.JWT_TOKEN_EXPIRATION);

    if (!payload || typeof payload !== 'object') {
      throw new ErrorService.Error(new Error('Invalid payload should be object'));
    }

    if (isNaN(ttl)) {
      throw new ErrorService.Error(new Error('Invalid ttl should be number'));
    }

    return true;
  },
};
