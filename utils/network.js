/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-loop-func */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
const os = require('os');

const networkInterfaces = os.networkInterfaces();

module.exports = {
  // Get Current NetWork IP Address
  getNetworkAddress: () => {
    let serverAddress = null;
    for (const faces in networkInterfaces) {
      networkInterfaces[faces].forEach((details) => {
        if ((details.family == 'IPv4') && !details.internal) {
          serverAddress = details.address;
        }
      });
    }
    return serverAddress;
  },
};
