require('dotenv').config();

const port = process.env.APP_PORT || 2345;
const koa = require('./config/koa');
const mongoose = require('./config/mongoose');
const { print } = require('./config/pino');

const mongo = mongoose;

const app = koa.init();
// Listen
const server = app.listen(Number(port), () => {
  mongo.startMongo();
  print(`Service started on port ${port}`);
  print('Server running at http://127.0.0.1:%d/', port);
});

module.exports = {
  app: server,
  mongo,
};
