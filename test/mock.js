module.exports = {
  user: {
    name: 'munish',
    email: 'manishkpr@gmail.com',
    emailUser1: 'manishkpr1998@gmail.com',
    inValidEmail: 'manishkpr..email@gmail.com',
    password: 'password',
    inValidPassword: 'password123',
    hashPassword: '$2a$10$1m6Z29gIPJ56u.X4Pz7Pd.hlVc6PPVcnEETqC8ExqDDyQf9OJeHJu',
  },
  payload: {
    name: 'munish',
    email: 'manishkpr@gmail.com',
  },
};
