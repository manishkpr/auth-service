/* eslint-disable no-undef */

process.env.NODE_ENV = 'test';

const httpStatus = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');

const {
  name, email, inValidEmail, password,
} = require('../mock').user;

const { app, mongo } = require('../../server');

const { assert } = chai;

chai.use(chaiHttp);

const path = '/createUser';

describe('/createUser /', async () => {
  beforeEach(async () => {
    if (mongo.getMongoConnection().collection('users').countDocuments()) {
      mongo.getMongoConnection().collection('users').deleteMany({});
    }
  });

  it('should return 500 if name', async () => {
    chai.request(app)
      .post(path)
      .end((err, res) => {
        const { code, status, detail } = res.body.error;
        assert.equal(status, httpStatus.BAD_REQUEST);
        assert.equal(code, 'C-1010');
        assert.equal(detail, 'Mandatory request parameter is missing: \'name\'');
      });
  });

  it('should return 500 if email is missing', async () => {
    chai.request(app)
      .post(path)
      .set('Content-Type', 'application/json')
      .send({ name, password })
      .end((err, res) => {
        const { code, status, detail } = res.body.error;
        assert.equal(status, httpStatus.BAD_REQUEST);
        assert.equal(code, 'C-1010');
        assert.equal(detail, 'Mandatory request parameter is missing: \'email\'');
      });
  });

  it('should return 500 if password is missing', async () => {
    chai.request(app)
      .post(path)
      .set('Content-Type', 'application/json')
      .send({ email, name })
      .end((err, res) => {
        const { code, status, detail } = res.body.error;
        assert.equal(status, httpStatus.BAD_REQUEST);
        assert.equal(code, 'C-1010');
        assert.equal(detail, 'Mandatory request parameter is missing: \'password\'');
      });
  });

  it('should return 500 if invalid email is provided', async () => {
    chai.request(app)
      .post(path)
      .set('Content-Type', 'application/json')
      .send({ name, email: inValidEmail, password })
      .end((err, res) => {
        const { code, status, detail } = res.body.error;
        assert.equal(status, httpStatus.BAD_REQUEST);
        assert.equal(code, 'C-1011');
        assert.equal(detail, 'Invalid email: \'email\'');
      });
  });

  it('should return 200 if valid name, email, password is provided', async () => {
    chai.request(app)
      .post(path)
      .set('Content-Type', 'application/json')
      .send({ name, email, password })
      .end((err, res) => {
        const expectedName = res.body.data.userResponse.name;
        const expectedEmail = res.body.data.userResponse.email;
        const { status } = res;
        assert.equal(status, httpStatus.OK);
        assert.equal(name, expectedName);
        assert.equal(email, expectedEmail);
      });
  });

  it('should throw error if user already exist', async () => {
    setTimeout(() => {
      chai.request(app)
        .post(path)
        .set('Content-Type', 'application/json')
        .send({ name, email, password })
        .end((err, res) => {
          const { code, status, detail } = res.body.error;
          assert.equal(status, httpStatus.BAD_REQUEST);
          assert.equal(code, 'C-1012');
          assert.equal(detail, `Already exist: '${email}'`);
        });
    }, 100);
  });
});
