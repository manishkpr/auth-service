/* eslint-env mocha */
const { assert } = require('chai');

const { mongo } = require('../../server');
const { redisClient } = require('../../config/redis');

describe('Z close connections', () => {
  it('should should close connection', async () => {
    assert.isTrue(true);
  });

  after((done) => {
    setTimeout(() => {
      if (mongo) {
        mongo.stopMongo();
      }
      if (redisClient) {
        redisClient.quit();
      }
    }, 300);
    done();
  });
});
