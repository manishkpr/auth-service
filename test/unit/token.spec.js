/* eslint-env mocha */

const { assert } = require('chai');
const Token = require('../../api/controllers/token');
const { payload } = require('../mock');

describe('Token', () => {
  let token;

  describe('generateToken', () => {
    it('should generate token if valid payload provided', async () => {
      const newtToken = await Token.generateToken(payload);
      token = newtToken;
      assert.isNotEmpty(token);
    });

    it('should not generate token if invalid payload provided', async () => {
      let exception;
      try {
        await Token.generateToken('payload');
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Invalid payload should be object');
    });
  });

  describe('verifyToken', () => {
    it('should verify token if available in redis cache', async () => {
      const isValidToken = await Token.verifyToken(token);
      assert.deepEqual(JSON.stringify(payload), isValidToken);
    });

    it('should not verify token if invalid token provided', async () => {
      let exception;
      try {
        await Token.verifyToken('');
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Invalid parameter: key');
    });

    it('should not verify token if not available in redis cache', async () => {
      const isValidToken = await Token.verifyToken('invalid token');
      assert.isNull(isValidToken);
    });
  });
});
