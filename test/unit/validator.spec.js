/* eslint-env mocha */

const { assert } = require('chai');
const Validator = require('../../utils/validator');

const {
  name, email, inValidEmail, password,
} = require('../mock').user;

describe('Validator', () => {
  describe('validateUserInfo', () => {
    it('should throw exception if name is missing', async () => {
      let exception;
      try {
        await Validator.validateUserInfo({ email, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Mandatory request parameter is missing: \'name\'');
    });

    it('should return true if name is ignored', async () => {
      const isValid = await Validator.validateUserInfo({ email, password }, { ignoreName: true });
      assert.isTrue(isValid);
    });

    it('should throw exception if email is missing', async () => {
      let exception;
      try {
        await Validator.validateUserInfo({ name, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Mandatory request parameter is missing: \'email\'');
    });

    it('should throw exception if password is missing', async () => {
      let exception;
      try {
        await Validator.validateUserInfo({ name, email });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Mandatory request parameter is missing: \'password\'');
    });

    it('should throw exception if invalid email is provided', async () => {
      let exception;
      try {
        await Validator.validateUserInfo({ name, email: inValidEmail, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Invalid email: \'email\'');
    });
  });

  describe('validatePayloadAndTTL', () => {
    it('should throw exception if payload is undefined', async () => {
      let exception;
      try {
        await Validator.validatePayloadAndTTL(undefined);
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Invalid payload should be object');
    });

    it('should throw exception if payload is null', async () => {
      let exception;
      try {
        await Validator.validatePayloadAndTTL(null);
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Invalid payload should be object');
    });

    it('should throw exception if payload is not object', async () => {
      let exception;
      try {
        await Validator.validatePayloadAndTTL('payload');
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Invalid payload should be object');
    });

    it('should not throw exception if payload is a object', async () => {
      const validate = await Validator.validatePayloadAndTTL({ name, email, password });
      assert.isTrue(validate);
    });

    it('should throw exception if invalid ttl is provided', async () => {
      let exception;
      process.env.JWT_TOKEN_EXPIRATION = 'invalid';
      try {
        await Validator.validatePayloadAndTTL({ name, email, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Invalid ttl should be number');
    });
  });
});
