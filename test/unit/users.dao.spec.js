/* eslint-env mocha */

const { assert } = require('chai');
const Users = require('../../api/controllers/dao/users');

const {
  name, email, emailUser1, inValidEmail, password, inValidPassword,
} = require('../mock').user;

describe('Users Dao', () => {
  describe('isUserExist', () => {
    it('should return user if user is exist', async () => {
      const user = await Users.isUserExist(email);
      assert.equal(user.email, email);
    });

    it('should return false if user not found', async () => {
      const isUserExist = await Users.isUserExist(inValidEmail);
      assert.isFalse(isUserExist);
    });

    it('should throw exception if invalid param provided', async () => {
      let exception;
      try {
        await Users.isUserExist({});
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: CastError: Cast to string failed for value "{}" at path "email" for model "User"');
    });
  });

  describe('createUser', () => {
    it('should throw exception if user already exist', async () => {
      let exception;
      try {
        await Users.createUser({ name, email, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, `Already exist: '${email}'`);
    });

    it('should create user if valid credential provided', async () => {
      const user = await Users.createUser({ name, email: emailUser1, password });
      assert.hasAllKeys(user, ['userResponse', 'token']);
      assert.equal(user.userResponse.email, emailUser1);
      assert.equal(user.userResponse.name, name);
      assert.isNotEmpty(user.token);
    });

    it('should throw exception if empty param provided', async () => {
      let exception;
      try {
        await Users.createUser({ });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Illegal arguments: undefined, string');
    });
  });

  describe('signIn', () => {
    it('should not throw exception if user sign in with valid credential', async () => {
      const user = await Users.signIn({ email, password });
      assert.hasAllKeys(user, ['userResponse', 'token']);
      assert.equal(user.userResponse.email, email);
      assert.equal(user.userResponse.name, name);
      assert.isNotEmpty(user.token);
    });

    it('should throw exception if user sign in with invalid email', async () => {
      let exception;
      try {
        await Users.signIn({ inValidEmail, password });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'User/Email not found');
    });

    it('should throw exception if user sign in with invalid password', async () => {
      let exception;
      try {
        await Users.signIn({ email, password: inValidPassword });
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, `Invalid User/Password: '${email}'`);
    });

    it('should throw exception if empty param provided', async () => {
      let exception;
      try {
        await Users.signIn();
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Cannot destructure property `email` of \'undefined\' or \'null\'.');
    });
  });
});
