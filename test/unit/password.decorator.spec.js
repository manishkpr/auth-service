/* eslint-env mocha */

const { assert } = require('chai');
const { decorate, comparePassword } = require('../../api/controllers/decorator/password');

const { password, inValidPassword, hashPassword } = require('../mock').user;

describe('Password decorator', () => {
  describe('decorate', () => {
    it('should decorate password with hash', async () => {
      const decoratedPassword = await decorate(password);
      assert.notEqual(password, decoratedPassword);
    });
  });

  describe('comparePassword', () => {
    it('should return true if valid password is provided', async () => {
      const isValidPassword = await comparePassword(password, hashPassword);
      assert.isTrue(isValidPassword);
    });

    it('should return false if invalid password is provided', async () => {
      const isValidPassword = await comparePassword(inValidPassword, hashPassword);
      assert.isFalse(isValidPassword);
    });

    it('should throw exception if invalid password is provided', async () => {
      let exception;
      try {
        await comparePassword({}, hashPassword);
      } catch (e) {
        exception = e;
      }
      assert.include(exception.message, 'Error occurred :: Error: Illegal arguments: object, string');
    });
  });
});
