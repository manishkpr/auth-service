/* eslint-env mocha */

const { assert } = require('chai');
const Users = require('../../api/controllers/decorator/users');

const user = require('../mock');

describe('Users decorator', () => {
  describe('decorate', () => {
    it('should decorate with specific fields', async () => {
      const decoratedUser = await Users.decorate(user);
      assert.deepEqual({ name: user.name, email: user.email }, decoratedUser);
    });
  });
});
