# Auth Service 

Simple Auth Service 


## Frameworks and Libraries

- Koa 2
- Mocha, Chai
- slint-airbnb
- mongoose (MongoDB)
- paspport
- redis


## Installation

#### Node JS

- Install Node [v8.7.0](https://nodejs.org/download/release/v8.7.0/) and above
To ensure that you're running the correct node version, please run the following command below to check installed version of node:

  ```
  node -v
  ```
- Or you can use NVM (Node version manager). Check [Setup Tutorial](https://medium.com/@onexlab.io/node-js-installation-best-practise-mac-os-2c7bdc6ef679)

#### MongoDB
- Install [Latest MongoDB](mongodb.com/download-center/community) 

#### Redis
- Install [Latest Redis](https://redis.io/download)

- Or use `Docker` to set up `MongoDB` as well `Redis`.

If you are `Windows/Mac` user please install `Docker/Docker Desktop` from the following links

- [Windows OS](https://docs.docker.com/docker-for-windows/install/)
- [Mac OS](https://docs.docker.com/docker-for-mac/install/)

To start `MongoDB` and `Redis` using `Docker` open `terminal/cmd prompt` then please run the following command:
    
    $ docker-compose up -d

Above command will install and run `MongoDB` and `Redis` in the `Docker Container`

To stop `Docker` container please run the following command:
    
    $ docker-compose down

 Above command will stop running `Docker Container`


`Note:` Make sure you are in the root directory of the project.

## Running

Make sure you have installed and running `Node JS`, `MongoDB` and `Redis` on your system and you are in the root directory of the project.

#### NPM (Node Package Manager)
Go to In the `root directory of the project`, please run the following command to install the `dependencies`:

    $ npm install

#### Environment Configuration

In the root directory, there is `.env` file which contains the following `Environment variables`.

If you have installed `MongoDB and Redis manually` then please change the following `.env` file variables values based on your `MongoDB and Redis`. 

`Note:` If you are running `MongoDB` and `Redis from `Docker container` then no need to change the following file.

    
    # APP
	APP_PORT=3000
	
	# NODE CONF

	LOG_ENABLED=true

	# MONGO DB CONF

	MONGO_HOST=localhost
	MONGO_PORT=27017
	MONGO_USER=api_user
	MONGO_PASSWORD=api1234

	# MONGO DEV DB
	MONGO_DB=api_dev_db
	
	# MONGO TEST DB
	MONGO_TEST_DB=api_test_db 

	# REDIS CONF
	
	REDIS_HOST=127.0.0.1
	REDIS_PORT=6379

	# JWT
	
	JWT_TOKEN_SECRET=auth-app

	# 24 HRS

	JWT_TOKEN_EXPIRATION=86400



#### Unit/Integration Test
To run `Unit/Integration` tests, please run the following command

    npm run test

Make sure all the tests are passing.

#### Start Service
To start service, please run the following command

    npm run start

#### Service End Points

##### 1. Create User

	curl --request POST \ --url http://127.0.0.1:3000/createUser \
	  --header 'content-type: application/json' \
	  --data '{ 
		"name": "test", 
		"email": "test@gmail.com", 
		"password": "test1234"
	}'

will return you the following `JSON response`

```JSON
{
  "data": {
    "userResponse": {
      "name": "test",
      "email": "test@gmail.com",
      "created": "2020-03-08T18:38:07.850Z"
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidGVzdCIsImVtYWlsIjoidGVzdEBnbWFpbC5jb20iLCJjcmVhdGVkIjoiMjAyMC0wMy0wOFQxODozODowNy44NTBaIiwiaWF0IjoxNTgzNjkyNjg3LCJleHAiOjE1ODM2OTI3NzN9.Sf4jW07dEnUpcJPB9nXhuXCHW2XkaYJy1jNuPBtV4xE"
  }
}
```
