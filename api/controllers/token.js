const jwt = require('jsonwebtoken');
const RedisCache = require('../controllers/dao/RedisCache');

const { validatePayloadAndTTL } = require('../../utils/validator');

async function generateToken(payload) {
  await validatePayloadAndTTL(payload);
  const token = jwt.sign(payload, process.env.JWT_TOKEN_SECRET,
    { expiresIn: process.env.JWT_TOKEN_EXPIRATION });

  await RedisCache.setex(token, process.env.JWT_TOKEN_EXPIRATION, JSON.stringify(payload));


  return token;
}

async function verifyToken(token) {

  const isValidToken = await RedisCache.get(token);

  return isValidToken;
}

module.exports = {
  generateToken,
  verifyToken,
};
