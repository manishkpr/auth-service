const bcrypt = require('bcryptjs');
const ErrorService = require('../../../services/ErrorService');

const saltRounds = 10;

module.exports = {
  async decorate(password) {
    try {
      const hash = bcrypt.hashSync(password, saltRounds);
      return hash;
    } catch (err) {
      throw new ErrorService.Error(err);
    }
  },

  async comparePassword(password, hash) {
    try {
      return bcrypt.compareSync(password, hash);
    } catch (err) {
      throw new ErrorService.Error(err);
    }
  },
};
