
module.exports = {
  async decorate(user) {
    return { name: user.name, email: user.email };
  },
};
