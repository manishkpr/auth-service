const User = require('../../models/users');
const { decorate, comparePassword } = require('../decorator/password');
const usrDecorator = require('../decorator/users');

const { generateToken } = require('../token');

// Service
const ErrorService = require('../../../services/ErrorService');

async function isUserExist(email) {
  try {
    const user = await User.findOne({ email });
    return user || false;
  } catch (err) /* istanbul ignore next */ {
    throw new ErrorService.Error(err);
  }
}

async function createUser(param) {
  const { name, email, password } = param;
  const hashPassword = await decorate(password);

  if (await isUserExist(email)) {
    throw new ErrorService.UserAlreadyExist(email);
  }

  try {
    const newUser = new User({
      provider: 'local',
      name,
      email,
      password: hashPassword,
    });

    const user = await newUser.save();
    const userResponse = await usrDecorator.decorate(user);

    const token = await generateToken(userResponse);

    return { userResponse, token };
  } catch (err) /* istanbul ignore next */ {
    throw new ErrorService.Error(err);
  }
}

async function signIn(param) {
  const { email, password } = param;

  const user = await isUserExist(email);

  if (!user) {
    throw new ErrorService.UserNotFound(email);
  }

  const isValidCredential = await comparePassword(password, user.password);

  if (!isValidCredential) {
    throw new ErrorService.InvalidUserPassword(email);
  }

  try {
    const userResponse = await usrDecorator.decorate(user);
    const token = await generateToken(userResponse);

    return { userResponse, token };
  } catch (err) /* istanbul ignore next */ {
    throw new ErrorService.Error(err);
  }
}

module.exports = { createUser, isUserExist, signIn };
