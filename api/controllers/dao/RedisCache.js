const { redisClient } = require('../../../config/redis');

// Services
const ErrorService = require('../../../services/ErrorService');

module.exports = {

  get: async (key) => {
    if (!key) throw new ErrorService.RedisInvalidParamError('key');

    try {
      const data = await redisClient.getAsync(key);
      return data;
    } catch (err) {
      throw new ErrorService.RedisError(err);
    }
  },

  setex: async (key, ttl, value) => {
    if (!key) throw new ErrorService.RedisInvalidParamError('key');
    if (!ttl) throw new ErrorService.RedisInvalidParamError('ttl');
    if (!value) throw new ErrorService.RedisInvalidParamError('value');

    try {
      const data = await redisClient.setexAsync(key, ttl, value);
      return data;
    } catch (err) {
      throw new ErrorService.RedisError(err);
    }
  },

};
