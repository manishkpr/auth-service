const UserDao = require('./dao/users');
const { validateUserInfo } = require('../../utils/validator');

async function createUser(ctx) {
  const param = ctx.request.body;

  await validateUserInfo(param);
  const response = await UserDao.createUser(param);

  return response;
}

async function signIn(ctx) {
  const param = ctx.request.body;

  await validateUserInfo(param, { ignoreName: true });
  const response = await UserDao.signIn(param);

  return response;
}

module.exports = {
  createUser,
  signIn,
};
