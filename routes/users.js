/* eslint-disable no-return-assign */
const Router = require('koa-router');
const UserController = require('../api/controllers/users');

const router = Router();

router.post('/createUser', async (ctx) => {
  const res = await UserController.createUser(ctx);
  ctx.body = { data: res };
});

module.exports = router;
